process MMseqs2_taxonomy_assignment_gene {
    debug true
    tag "$meta.id"

    input:
    tuple val(meta), path(query)
    tuple val(db)

    output:
    tuple val(meta), path("*.tsv"), path("*.html"), path("*report"), emit: taxonomy_result
    tuple val(meta), path("*.tsv")                                 , emit: taxonomy_tsv
    path('*.log')                                                   , emit: log

    script:
    def args = task.ext.args ?: ''
    def prefix = "${params.mmseqs_db}".split("/")[-1]
    """
    mkdir tmp
    mkdir result
    echo ${params.mmseqs_db}
    echo "to createdb for $query by the name ${meta.id}_queryDB "
    echo $prefix
    echo $db

    mmseqs createdb $query ${meta.id}_queryDB
    mmseqs taxonomy ${meta.id}_queryDB ${params.mmseqs_db}  result/${meta.id}_result tmp --threads ${params.max_cpus}
    mmseqs createtsv ${meta.id}_queryDB result/${meta.id}_result ${meta.id}_taxonomy_gene.tsv


    mmseqs taxonomyreport ${params.mmseqs_db} result/${meta.id}_result ${meta.id}_gene_report
    mmseqs taxonomyreport ${params.mmseqs_db} result/${meta.id}_result ${meta.id}_taxonomy_gene.html --report-mode 1



    2> ${meta.id}_mmseqs_taxonomy_gene.log



    """

}