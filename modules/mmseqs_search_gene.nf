process MMseqs2_search_gene {
    debug true
    tag "$meta.id"

    input:
    tuple val(meta), path(query)
    val db

    output:
    tuple val(meta), path("*.tsv")                                 , emit: search_result
    path('*.log')                                                   , emit: log


    script:
    def args = task.ext.args ?: ''
    def prefix = "${params.mmseqs_search_db}".split("/")[-1]

    """
    mkdir tmp
    mkdir result

    mmseqs createdb $query ${meta.id}_queryDB
    mmseqs search ${meta.id}_queryDB ${params.mmseqs_search_db} result/${meta.id}_result tmp \\
    --exhaustive-search --start-sens 1 --sens-steps 3 -s 7 -e 1.000E-05  --max-accept 5 \\
    --threads ${params.max_cpus}

    mmseqs convertalis ${meta.id}_queryDB ${params.mmseqs_search_db} result/${meta.id}_result ${meta.id}_convertalis.tsv \\
    --format-output query,target,theader,pident,alnlen,mismatch,gapopen,qstart,qend,tstart,tend,evalue,bits,empty,qlen,tlen,taxid,taxname,taxlineage  \\
    --threads ${params.max_cpus}

    2> ${meta.id}_mmseqs_search_gene.log

    """

}