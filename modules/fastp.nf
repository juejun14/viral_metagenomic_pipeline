

process FastP{
    tag "$meta.id"
    debug true


    input:
    tuple  val(meta), path(fasta)
    val   save_trimmed_fail
    val   save_merged

    output:
    tuple val(meta), path('*.trim.fastq.gz')  , optional:true, emit: reads
    tuple val(meta), path('*.json')           , emit: json

    tuple val(meta), path('*.html')           , emit: html
    tuple val(meta), path('*.log')            , emit: log



    script:
    def args = task.ext.args ?: ''
    def args2 = task.ext.args2 ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"
    def fail_fastq  = save_trimmed_fail ? "--unpaired1 ${prefix}_1.fail.fastq.gz --unpaired2 ${prefix}_2.fail.fastq.gz" : ''
    def merge_fastq = save_merged ? "-m --merged_out ${prefix}.merged.fastq.gz" : ''

    """
    echo $fasta
    fastp \\
    --in1 ${fasta[0]} \\
    -w ${params.max_cpus} \\
    --in2 ${fasta[1]} \\
    --out1 ${prefix}_1.trim.fastq.gz \\
    --out2 ${prefix}_2.trim.fastq.gz \\
    --json ${prefix}.fastp.json \\
    --html ${prefix}.fastp.html \\
    --detect_adapter_for_pe \\
    $args \\
    $args2 \\
    2> ${prefix}.fastp.log
    """

}

