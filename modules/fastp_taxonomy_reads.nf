

process FastP_reads {
    tag "$meta.id"
    debug true


    input:
    tuple  val(meta), path(fasta)


    output:
    tuple val(meta), path('*.trim.fastq.gz')  , optional:true, emit: reads
    tuple val(meta), path('*.json')           , emit: json
    tuple val(meta), path('*.html')           , emit: html
    tuple val(meta), path('*.log')            , emit: log
    tuple val(meta), path('*.merged.fastq.gz'), emit: merged


    script:
    def args = task.ext.args ?: ''
    def args2 = task.ext.args2 ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"


    """
    echo $fasta
    fastp \\
    --in1 ${fasta[0]} \\
    -w ${params.max_cpus} \\
    --in2 ${fasta[1]} \\
    --json ${prefix}.fastp_cleaned.json \\
    --html ${prefix}.fastp_cleaned.html \\
    -m --merged_out ${prefix}.merged.fastq.gz \\
    $args \\
    $args2 \\
    1> ${prefix}.fastp.log
    """

}