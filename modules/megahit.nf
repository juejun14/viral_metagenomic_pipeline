process MEGAHIT{
    debug true
    tag "$meta.id"

    input:
    tuple val(meta), path(reads)

    output:
    tuple val(meta), path("MEGAHIT-${meta.id}_contig.fasta"), emit: assembly
    tuple val(meta), path("MEGAHIT-${meta.id}_contig_1000.fasta"), emit: assembly_1000
    path "log"                                           , emit: log
    path "options.json"                                  , emit: options


    script:
    def args = task.ext.args ?: ''
    """
    megahit -1 ${reads[0]} -2 ${reads[1]} \
        $args \
        -o megahit

    mv megahit/final.contigs.fa final.contigs.fa
    mv megahit/log log
    mv megahit/options.json options.json

    seqtk rename final.contigs.fa contig > MEGAHIT-${meta.id}_contig.fasta
    seqtk seq -L 1000 MEGAHIT-${meta.id}_contig.fasta > MEGAHIT-${meta.id}_contig_1000.fasta
    gzip -c "MEGAHIT-${meta.id}_contig.fasta" > "MEGAHIT-${meta.id}_contig.fasta.gz"

    """



}