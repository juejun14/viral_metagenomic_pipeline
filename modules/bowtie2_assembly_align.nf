process Bowtie2_assembly_align {
    debug true
    tag "$meta.id"


    input:
    tuple val(assembly_meta), path(assembly_index)
     /*val(reads_meta), path(reads)*/
    tuple val(meta), path(reads)

    output:
    tuple val(assembly_meta), path("*.bam"), path("*.bam.bai"), emit: mappings
    tuple val(assembly_meta), path("*.bowtie2.log")  ,          emit: log
    tuple val(assembly_meta), path("*.txt"),                    emit: report
    tuple val(assembly_meta), path("*.png"),                    emit: plot


    script:
    def args = task.ext.args ?: ''
    def input = "-1 ${reads[0]} -2 ${reads[1]}"
    def prefix = task.ext.prefix ?:  "${meta.id}"

    """
    echo $assembly_index
    INDEX=`find -L ./ -name "*.rev.1.bt2l" -o -name "*.rev.1.bt2" | sed 's/.rev.1.bt2l//' | sed 's/.rev.1.bt2//'`

    bowtie2 \\
        -p ${params.max_cpus}  \\
        -x ${assembly_index[0].getSimpleName()} \\
        $args \\
        $input \\
        2> "${prefix}_assembly.bowtie2.log" | \
        samtools view -@ ${params.max_cpus} -bS | \
        samtools sort -@ ${params.max_cpus} -o "${prefix}_assembly.bam" \

    samtools index "${prefix}_assembly.bam"

    samtools depth -@ ${params.max_cpus} ${prefix}_assembly.bam -o ${prefix}_assembly.depth.txt
    samtools coverage -d -r  ${prefix}_assembly.bam -o ${prefix}_assembly.coverage.txt

    python ${baseDir}/assets/script/depth_contig.py ${prefix}_assembly.depth.txt ${meta.id} ${prefix}_assembly.depth_plot.png
    python ${baseDir}/assets/script/coverage_visualization.py ${prefix}_assembly.coverage.txt ${meta.id} ${prefix}_assembly.coverage_plot.png

    """

}