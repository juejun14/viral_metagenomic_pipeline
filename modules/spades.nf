process SPADES {
    tag "$meta.id"

    input:
    tuple val(meta), path(reads)

    output:
    tuple val(meta), path("SPAdes-${meta.id}_scaffolds.fasta"), emit: assembly
    path "SPAdes-${meta.id}.log"                              , emit: log
    path "SPAdes-${meta.id}_contigs.fasta.gz"                 , emit: contigs_gz
    path "SPAdes-${meta.id}_scaffolds.fasta.gz"               , emit: assembly_gz
    path "SPAdes-${meta.id}_scaffolds_rename.fasta.gz"
    tuple val(meta), path("SPAdes-${meta.id}_scaffolds_rename.fasta"), emit: assembly_rename

    script:
    def args = task.ext.args ?: ''
    /*maxmem = task.memory.toGiga()##     --memory $maxmem \*/
    """
    echo $args
    mkdir spades
    spades.py \
        $args \
        -m ${params.max_memory} \
        --threads ${params.max_cpus}  \
        --pe1-1 ${reads[0]} \
        --pe1-2 ${reads[1]} \
        -k 35 \
        -o spades



    mv spades/scaffolds.fasta SPAdes-${meta.id}_scaffolds.fasta
    mv spades/spades.log SPAdes-${meta.id}.log
    mv spades/contigs.fasta SPAdes-${meta.id}_contigs.fasta

    seqtk rename SPAdes-${meta.id}_scaffolds.fasta contig >  SPAdes-${meta.id}_scaffolds_rename.fasta
    awk -F "_" '/^>/{type=substr(\$0, index(\$0, \"type_\")+5); printf(\">contig%d type_%s\\n\", ++i, type)} /^([^>])/ {print}'  SPAdes-${meta.id}_scaffolds.fasta > "SPAdes-${meta.id}_scaffolds_rename_with_type.fasta"


    gzip "SPAdes-${meta.id}_contigs.fasta"
    gzip -c "SPAdes-${meta.id}_scaffolds_rename_with_type.fasta" > "SPAdes-${meta.id}_scaffolds.fasta.gz"
    gzip -c "SPAdes-${meta.id}_scaffolds_rename.fasta" > "SPAdes-${meta.id}_scaffolds_rename.fasta.gz"




    """


}
