process PROKKA {
    tag "$meta.id"
    debug true

    input:
    tuple val(meta), path(assembly)

    output:
    tuple val(meta), path("*.gff"), emit: gff
    tuple val(meta), path("*.gbk"), emit: gbk
    tuple val(meta), path("*.fna"), emit: fna
    tuple val(meta), path("*.faa"), emit: faa
    tuple val(meta), path("*.rename.faa"), emit: rename_faa
    tuple val(meta), path("*.ffn"), emit: ffn
    tuple val(meta), path("*.sqn"), emit: sqn
    tuple val(meta), path("*.fsa"), emit: fsa
    tuple val(meta), path("*.tbl"), emit: tbl
    tuple val(meta), path("*.err"), emit: err
    tuple val(meta), path("*.log"), emit: log
    tuple val(meta), path("*.txt"), emit: txt
    tuple val(meta), path("*.tsv"), emit: tsv


    script:
    def args = task.ext.args   ?: ''
    prefix   = task.ext.prefix ?: "${meta.id}"

    """
    seqtk rename $assembly contig >  ${meta.id}_rename.fasta
    prokka ${meta.id}_rename.fasta \\
        --force --addgenes --addmrna \\
        --metagenome \\
        --evalue 1e-05 \\
        --cpus ${params.threads} \\
        --prefix $prefix \\
        --outdir $prefix/

    python ${baseDir}/assets/script/rename_prokka.py \\
    ${prefix}/${prefix}.faa \\
    ${prefix}/${prefix}.gff \\
    ${prefix}/${prefix}.rename.faa

    mv $prefix/* .

    """

}