process Bowtie2_removal_build {
    tag "$fasta"

    input:
    path fasta

    output:
    path 'bt2_index_base*', emit: index

    script:
    """
    bowtie2-build --threads ${params.max_cpus} $fasta "bt2_index_base"
    """
}
