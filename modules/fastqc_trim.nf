process FastQC_Trim {
    debug true
    tag "$meta.id"

    input:
    tuple  val(meta), path(reads)

    output:
    tuple val(meta), path("*.html"), emit: html
    tuple val(meta), path("*.zip"), emit: zip
    path '*.log'

    script:
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"

    """
    fastqc ${reads[0]} ${reads[1]} -o . \\
    $args \\

    2> ${prefix}.fastqc_trim.log

    """

}