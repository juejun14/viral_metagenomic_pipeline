process MMseqs2_taxonomy_assignment_reads {
    debug true
    tag "$meta.id"

    input:
    tuple val(meta), path(query)
    tuple val(db)

    output:
    tuple val(meta), path("*.html"), path("result_reads/*report*")                , emit: taxonomy_result
    path('*.log')                                                   , emit: log

    script:
    def args = task.ext.args ?: ''
    def prefix = "${params.mmseqs_db}".split("/")[-1]
    """
    mkdir tmp
    mkdir result_reads
    echo ${params.mmseqs_db}
    echo $args
    echo $prefix
    echo $query

    mmseqs easy-taxonomy $query $db result_reads/${meta.id}_report tmp \\
    $args \\
    --threads ${params.max_cpus}

    ktUpdateTaxonomy.sh taxonomy
    ktImportTaxonomy result_reads/${meta.id}_report_report -tax taxonomy \\
    -o ${meta.id}_taxonomy_reads.html -t 5 -m 3

    2> ${meta.id}_mmseqs_reads.log

    """
}