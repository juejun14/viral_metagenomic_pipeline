process QUAST {
    debug true
    tag "$meta.id"

    input:
    tuple val(meta), path(assembly)

    output:
    path "*", emit: quast

    script:
    """
    metaquast.py $assembly --threads ${params.threads} \
    --rna-finding --max-ref-number 0 -o .
    """

}