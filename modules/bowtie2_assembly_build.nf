process Bowtie2_assembly_build {

    input:
    tuple val(meta), path(assembly)

    output:
    tuple val(meta), path('bt2_index_base*'), emit: assembly_index

    script:
    """
    mkdir bowtie

    bowtie2-build --threads ${params.max_cpus} $assembly "bt2_index_base"
    """
}