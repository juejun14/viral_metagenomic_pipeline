process HELP_DOC {
    debug true

    script:
    """
    cat  <<EOF

    --------------------------------------------------------------------
    |                                                                  |
    |           \033[1mThe pipeline of viral metagenomic analysis\033[0m             |
    |                                                                  |
    --------------------------------------------------------------------

    Pipeline developped by Juejun CHEN. 2023


    Usage: nextflow run main.nf --input PE_reads.fastq.gz --outdir [options]

    --input     [string]  Input your PE reads files: "reads.R{1,2}.*.fastq.gz"
    --outdir    [string]  Define your output directory to save the result


    Options:

    \033[1m\033[4mQuality control\033[24m\033[0m

        --fastp_options  [string]  Add classic fastp options.
                                   (Default: " --length_required 15 -q 15 --cut_front --cut_tail --cut_mean_quality 15 ")
        --fastqc_options [string]  Add classic fastqc options. (Default: null)

    For more information, please check fastp or fastqc documentation.


    \033[1m\033[4mAssembly\033[24m\033[0m

        --keep_phix    [boolean] This option used to unable the removal of the bacteriophage genome from illumina sequencing reads (Default: false)
        --host_fasta   [string] Remove the host genome from raw reads (Default: null)
        --bowtie2_mode [string] Bowtie2 options (Default: null)

    For more information, please check the bowtie2 documentation.


        --skip_spades    [boolean] Skip the assembly steps (Default: false)
        --spades_optons  [string] Choose the type of assembly (Default: "--metaviral").
                                  For the rna dataset, you should add "--rnaviral" as option)

        --skip_quast     [boolean] Skip the quality control of assembly by quast steps (Default: false)

        --skip_prokka    [boolean] Skip the gene prediction by prokka steps (Default: false)

    \033[1m\033[4mTaxonomy assignment\033[24m\033[0m

        --skip_mmseqs_reads     [boolean] Skip the taxonomy assignment step for raw reads.
                                          Attention, this step will take a lot of time (Default: true)
        --skip_mmseqs_gene      [boolean] Skip the taxonomy assignment step for the predicted gene.  (Default: false)

        --mmseqs_db  [string] The database formatted for mmseqs taxonomy assignment.
                              (Default: Uniref90 filtered in viropip projet on IFB:
                              /shared/projects/viropip/mmseq2DB/uniref90_cleaned_DB_part_14)

        --mmseqs_reads_options [string]  Add mmseqs2 options for reads.
                                         (Default: "-s 7 --min-length 100 --max-accept 5 --max-seqs 100 ")

        --mmseqs_gene_options  [string] Add mmseqs2 options for predicted gene.
                                         (Default: null )


    \033[1m\033[4mAnnotation of gene\033[24m\033[0m

        --skip_mmseqs_search   [boolean] Skip the search step by mmseqs for predicted gene.

        --mmseqs_search_db     [string] The database formatted for mmseqs search.
                                        (Default: Swiss-prot DB in viropip projet on IFB:
                                        /shared/projects/viropip/swiss_prot/swissprot)

        --mmseqs_search_options [string] Add mmseqs2 search options for predicted gene.
                                         (Default: null)


        --skip_hmmer_search   [boolean] Skip the search step by hmmer for predicted gene.

        --hmmerDB     [string] The database formatted for hmmer search.
                                        (Default: Pfam DB in viropip projet on IFB:
                                        /shared/projects/viropip/swiss_prot/swissprot)

        --hmmer_options [string] Add hmmer options for predicted gene.
                                         (Default: "-E 1e-5 --acc")


    For more information, please check mmseqs2 or hmmer documentation.


    \033[1m\033[4mBinning\033[24m\033[0m

        --skip_binning     [boolean] Skip the binning step. (Default: false)

        --metabat2_options [string] Add metabat2 options for binning
                                    (Default: "-m 1500" )

    For more information, please check metabat2 documentation.

    EOF
    """
}
