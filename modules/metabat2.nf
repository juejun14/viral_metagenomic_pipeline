process METABAT2 {
    debug true
    tag "$meta.id"

    input:
    tuple val(meta), path(contigs)
    tuple val(meta), path(bam), path(bai)

    output:
    tuple val(meta), path("*"),  emit:binning
    tuple val(meta), path("*.log"),         emit: log

    script:
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?:  "${meta.id}"

    """
    jgi_summarize_bam_contig_depths  --outputDepth ${meta.id}.depth.txt $bam
    metabat2 -t ${params.max_cpus} -i $contigs -a ${meta.id}.depth.txt \\
    -o ${meta.id}_binning -v \\
    $args \\
    1> ${meta.id}_binning_report.log \\
    2> ${meta.id}_binning_error.log

    """

}