process HMMER_SEARCH {
    debug true
    tag "$meta.id"

    input:
    tuple val(meta), path(query)

    output:
    tuple val(meta), path("*.tbl"),path("*.txt")                    , emit: search_result
    path('*.log')                                                   , emit: log


    script:
    def args = task.ext.args ?: ''
    def prefix = "${params.hmmerDB}".split("/")[-1]

    """
    mkdir hmmer_tmp
    cp ${params.hmmerDB} hmmer_tmp/
    hmmpress -f hmmer_tmp/$prefix

    hmmscan $args \\
    -o ${meta.id}_hmmscan_result.txt --tblout ${meta.id}_hmmscan_result.tbl \\
    --cpu ${params.max_cpus} \\
    hmmer_tmp/$prefix $query

    rm -r hmmer_tmp

    2> ${meta.id}_hmmscan.log
    """


}