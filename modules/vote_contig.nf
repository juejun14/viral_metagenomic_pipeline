process VOTE_CONTIGS {
    debug true
    tag "$meta.id"

    input:
    tuple val(meta), path(tsv_file)

    output:
    path "*", emit: vote_result

    script:
    """
    python ${baseDir}/assets/script/contig_vote.py $tsv_file -o ${meta.id}_contigs_type.xlsx

    """

}