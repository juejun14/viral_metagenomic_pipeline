#!/usr/bin/env nextflow
nextflow.enable.dsl=2

include { HELP_DOC } from './modules/help_doc'

include { FastQC } from './modules/fastqc'
include { FastP } from './modules/fastp'
include { FastP_reads } from './modules/fastp_taxonomy_reads'
include { FastQC_Trim } from './modules/fastqc_trim'

include { Bowtie2_removal_build as Bowtie2_removal_build_phix} from './modules/bowtie2_removal_build'
include { Bowtie2_removal_align as Bowtie2_removal_align_phix} from './modules/bowtie2_removal_align'
include { Bowtie2_removal_build as Bowtie2_removal_build_host} from './modules/bowtie2_removal_build'
include { Bowtie2_removal_align as Bowtie2_removal_align_host} from './modules/bowtie2_removal_align'

include { SPADES } from './modules/spades'
include { MEGAHIT } from './modules/megahit'



include { Bowtie2_assembly_build as Bowtie2_assembly_build_SPAdes} from './modules/bowtie2_assembly_build'
include { Bowtie2_assembly_align as Bowtie2_assembly_align_SPAdes} from './modules/bowtie2_assembly_align'
include { Bowtie2_assembly_build as Bowtie2_assembly_build_MEGAHIT} from './modules/bowtie2_assembly_build'
include { Bowtie2_assembly_align as Bowtie2_assembly_align_MEGAHIT} from './modules/bowtie2_assembly_align'

include { QUAST as QUAST_SPADES} from './modules/quast'
include { QUAST as QUAST_MEGAHIT } from './modules/quast'

include { PROKKA as PROKKA_SPAdes} from './modules/prokka'
include { PROKKA as PROKKA_MEGAHIT} from './modules/prokka'

include { MMseqs2_search_gene as MMseqs2_search_gene_SPAdes} from './modules/mmseqs_search_gene'
include { MMseqs2_search_gene as MMseqs2_search_gene_MEGAHIT} from './modules/mmseqs_search_gene'

include { HMMER_SEARCH as HMMER_SEARCH_SPAdes} from './modules/hmmer_search'
include { HMMER_SEARCH as HMMER_SEARCH_MEGAHIT} from './modules/hmmer_search'

include { MMseqs2_taxonomy_assignment_reads} from './modules/mmseqs_taxonomy_assignment_reads'

include { MMseqs2_taxonomy_assignment_gene as MMseqs2_taxonomy_assignment_gene_SPAdes} from './modules/mmseqs_taxonomy_assignment_gene'
include { MMseqs2_taxonomy_assignment_gene as MMseqs2_taxonomy_assignment_gene_MEGAHIT} from './modules/mmseqs_taxonomy_assignment_gene'

include { METABAT2 as METABAT2_SPAdes } from './modules/metabat2'
include { METABAT2 as  METABAT2_MEGAHIT } from './modules/metabat2'

include { VOTE_CONTIGS as VOTE_CONTIGS_SPAdes } from './modules/vote_contig'
include { VOTE_CONTIGS as VOTE_CONTIGS_MEGAHIT } from './modules/vote_contig'



workflow {

    /*
    ================================================================================
                                Documentation
    ================================================================================
    */

    if (params.help) {
        HELP_DOC()
    }

    else {
        /*
        ================================================================================
                                    Quality control
        ================================================================================
        */

        input_ch = Channel.fromFilePairs(params.input, size: params.single_end ? 1 : 2)
                          .view()
                          .ifEmpty { exit 1, "Cannot find any reads matching: ${params.input}\nNB: Path needs to be enclosed in quotes!" }
                .map { row ->
                            def meta = [:]
                            meta.id           = row[0]
                            meta.group        = 0
                            meta.single_end   = params.single_end
                            return [ meta, row[1] ]
                    }

        FastQC(input_ch)
        FastP( input_ch, params.fastp_save_trimmed_fail,[] )

        short_reads = FastP.out.reads

        if (params.host_fasta){
            ch_host_fasta = Channel
                .value(file( "${params.host_fasta}" ))

            Bowtie2_removal_build_host ( ch_host_fasta )
            bowtie2_host_ch = Bowtie2_removal_build_host.out.index
            Bowtie2_removal_align_host(short_reads, bowtie2_host_ch)
            short_reads = Bowtie2_removal_align_host.out.reads
        }

        if (!params.keep_phix) {

            bowtie2_phix_ch = Channel.value(file("${params.phix_reference}"))
            Bowtie2_removal_build_phix(bowtie2_phix_ch)
            Bowtie2_removal_align_phix(short_reads, Bowtie2_removal_build_phix.out.index)
            short_reads = Bowtie2_removal_align_phix.out.reads
        }

        if (!params.skip_mmseqs_reads && params.mmseqs_db) {
            FastP_reads(short_reads)
            merged_ch = FastP_reads.out.merged
            MMseqs2_taxonomy_assignment_reads(merged_ch, params.mmseqs_db)
        }

        FastQC_Trim(short_reads)

        /*
        ================================================================================
                                            Assembly (MEGAHIT)
        ================================================================================
        */
        if(!params.skip_megahit){
            MEGAHIT( short_reads)
            megahit_assembly = MEGAHIT.out.assembly_1000

            Bowtie2_assembly_build_MEGAHIT(megahit_assembly)
            Bowtie2_assembly_align_MEGAHIT(Bowtie2_assembly_build_MEGAHIT.out.assembly_index, short_reads )

            if (!params.skip_quast) {
                QUAST_MEGAHIT ( megahit_assembly )
            }
                if (!params.skip_prokka){
                    PROKKA_MEGAHIT ( megahit_assembly )
                    gene_ch = PROKKA_MEGAHIT.out.rename_faa


                    /*
                    ================================================================================
                                        Taxonomy assignment and Annotation of gene
                    ================================================================================
                    */

                    if (!params.skip_mmseqs_gene && params.mmseqs_db) {
                        MMseqs2_taxonomy_assignment_gene_MEGAHIT(gene_ch, params.mmseqs_db)
                        VOTE_CONTIGS_MEGAHIT(MMseqs2_taxonomy_assignment_gene_MEGAHIT.out.taxonomy_tsv)
                    }

                    if (!params.skip_mmseqs_search && params.mmseqs_search_db) {
                        MMseqs2_search_gene_MEGAHIT(gene_ch, params.mmseqs_search_db)
                    }

                    if (!params.skip_hmmer_search && params.hmmerDB) {
                        HMMER_SEARCH_MEGAHIT(gene_ch)
                    }

                }

                /*
                ================================================================================
                                            Binning preparation
                ================================================================================
                */
                if(!params.skip_binning){
                    METABAT2_MEGAHIT(megahit_assembly, Bowtie2_assembly_align_MEGAHIT.out.mappings)
                }

        }


        /*
        ================================================================================
                                         Assembly (SPAdes)
        ================================================================================
        */

        if(!params.skip_spades){
            SPADES( short_reads )
            spades_assembly = SPADES.out.assembly_rename

            Bowtie2_assembly_build_SPAdes(spades_assembly)
            Bowtie2_assembly_align_SPAdes(Bowtie2_assembly_build_SPAdes.out.assembly_index, short_reads )

            if (!params.skip_quast) {
                QUAST_SPADES ( spades_assembly )
            }

            if (!params.skip_prokka){
                PROKKA_SPAdes ( spades_assembly )
                gene_ch = PROKKA_SPAdes.out.rename_faa

                /*
                ================================================================================
                                    Taxonomy assignment and Annotation of gene
                ================================================================================
                */

                if (!params.skip_mmseqs_gene && params.mmseqs_db) {
                    MMseqs2_taxonomy_assignment_gene_SPAdes(gene_ch, params.mmseqs_db)
                    VOTE_CONTIGS_SPAdes(MMseqs2_taxonomy_assignment_gene_SPAdes.out.taxonomy_tsv)
                }

                if (!params.skip_mmseqs_search && params.mmseqs_search_db) {
                    MMseqs2_search_gene_SPAdes(gene_ch, params.mmseqs_search_db)
                }

                if (!params.skip_hmmer_search && params.hmmerDB) {
                    HMMER_SEARCH_SPAdes(gene_ch)
                }

            }

            /*
            ================================================================================
                                                  Binning
            ================================================================================
            */
            if(!params.skip_binning){
                METABAT2_SPAdes(spades_assembly, Bowtie2_assembly_align_SPAdes.out.mappings)
            }

        }



    }

}

