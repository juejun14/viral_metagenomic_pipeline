# -*- coding: utf-8 -*-
# @Time : 2023/5/24 20:15
# @Author : Juejun CHEN
import pandas as pd
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description='Data Visualization')
parser.add_argument('filename', type=str, help='Input data file name')
parser.add_argument('title', type=str, help='Title for the plot')
parser.add_argument('save', type=str, help='Output file name')

args = parser.parse_args()


def visualize_data(filename, title, save_filename):
    """
    Create a figure for coverage of contigs.

    :param filename: coverage data
    :param title: title for the figure
    :param save_filename: store the figure
    :return: coverage plot
    """

    # read file
    data = pd.read_csv(filename, delimiter='\t', header=None, names=['Contig', 'Length', 'Depth'])
    data1 = data.groupby('Contig')[['Depth']].mean()
    data2 = data.groupby('Contig')[['Length']].max()




    # extract the data to plot
    x1 = data2['Length']
    y1 = data1['Depth']


    # create plot
    plt.scatter(x1, y1, marker='o')

    # add title and label
    plt.title('Distribution of contig depth of ' + title)
    plt.xlabel('Contig length (bp)')
    plt.ylabel('Contig depth (X)')

    # save picture
    # data2.to_excel(save_filename)
    plt.savefig(save_filename)

visualize_data(args.filename, args.title, args.save)
