import argparse

# Set up the command line argument parser
parser = argparse.ArgumentParser(
    description='Rename protein sequence headers with associated contig and gene information')
parser.add_argument('input_faa', type=str, help='The input protein fasta file')
parser.add_argument('input_gff', type=str, help='The input GFF file')
parser.add_argument('output_faa', type=str, help='The output protein fasta file with renamed headers')
args = parser.parse_args()
# Parse the GFF file to get the contig and gene information
contigs = {}
genes = {}

gene_counts = {}

with open(args.input_gff, 'r') as gff_file:
    for line in gff_file:
        if not line.startswith('#'):
            parts = line.strip().split('\t')
            try:
                if parts[2] == 'CDS':
                    contig_id = parts[0]

                    gene_id = parts[8].split(';')[0].split('=')[1]

                    if contig_id not in contigs:
                        number = 1
                        formatted_number = "{:05d}".format(number)
                        contigs[contig_id] = {'start': int(parts[3]), 'end': int(parts[4]), 'length': 0,
                                              'gene_number': formatted_number}

                    else:
                        contigs[contig_id]['start'] = min(contigs[contig_id]['start'], int(parts[3]))
                        contigs[contig_id]['end'] = max(contigs[contig_id]['end'], int(parts[4]))
                        number += 1
                        formatted_number = "{:05d}".format(number)

                    genes[gene_id] = {'contig': contig_id, 'start': int(parts[3]), 'end': int(parts[4]),
                                      'length': int(parts[4]) - int(parts[3]) + 1, 'gene_number': formatted_number}

            except:
                pass

# Rename the protein sequence headers with the associated contig and gene information
with open(args.input_faa, 'r') as faa_file, open(args.output_faa, 'w') as out_file:
    for line in faa_file:
        if line.startswith('>'):
            parts = line.strip().split()
            gene_id = parts[0][1:]
            description = "_".join(parts[1:])
            gene_number = genes[gene_id]['gene_number']
            contig_id = genes[gene_id]['contig']
            start = genes[gene_id]['start']
            end = genes[gene_id]['end']
            length = genes[gene_id]['length']
            contig_length = contigs[contig_id]['end'] - contigs[contig_id]['start'] + 1

            new_header = f'>{contig_id}_{gene_number} {start}-{end}_{description}_gene-length{length}_cov-length{contig_length}'
            out_file.write(new_header + '\n')

        else:
            out_file.write(line)
