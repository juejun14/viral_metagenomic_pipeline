# -*- coding: utf-8 -*-
"""
@Time: 5/24/23 5:03 PM
@Author: Juejun CHEN
"""
import pandas as pd
import argparse
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser(description='Data Visualization')
parser.add_argument('filename', type=str, help='Input data file name')
parser.add_argument('title', type=str, help='Title for the plot')
parser.add_argument('save', type=str, help='Output file name')

args = parser.parse_args()


def visualize_data(filename, title, save_filename):
    """
    Create a figure for depth of contigs.

    :param filename: depth data
    :param title: title for the figure
    :param save_filename: store the figure
    :return: depth plot
    """
    # read file
    data = pd.read_csv(filename, delimiter='\t', header=0)

    y = data['coverage']
    x = data['endpos']

    # plt.bar(x, y)
    plt.scatter(x, y, marker='o')

    # add title
    plt.title('Distribution of coverage of ' + title)
    plt.ylabel('Coverage %')
    plt.xlabel('Contig length (bp)')
    plt.savefig(save_filename)


visualize_data(args.filename, args.title, args.save)


