import argparse
import pandas as pd
import requests
import xml.etree.ElementTree as ET
import time
import openpyxl


# Define the command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument('filename', help='The name of the TSV file to process')
parser.add_argument('-o', '--output', help='The name of the output file')

# Parse the command-line arguments
args = parser.parse_args()

# Load the table into a Pandas DataFrame
df = pd.read_csv(args.filename, delimiter='\t')
names=["sequence_id", "taxonomy_id", "rank", "scientific_name"]
df.columns=names


# Define a function to determine the organism type based on the taxonomy ID


def get_organism_type(taxon_id):
    # print(taxon_id)
    if taxon_id == 0 or taxon_id == 1:
        return "Unclassified"
    else:
        url = f"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&id={taxon_id}&retmode=xml"
        retries = 10
        while retries > 0:
            response = requests.get(url)
            if response.status_code == 200:
                break
            else:
                retries -= 1
                print(f"Retrying in 1 second... ({retries} retries left)")
                time.sleep(1)
        if response.status_code != 200:
            raise ValueError(f"Failed to retrieve taxonomy information for taxon ID {taxon_id}")
        root = ET.fromstring(response.content)

        scientific_name = ET.tostring(root.find("Taxon").find("ScientificName")).decode()
        lineage = root.find("Taxon").find("Lineage")
        organism_type = ET.tostring(lineage).decode()
        if "Viruses" in organism_type or "Viruses" in scientific_name:
            return "Viruses"
        elif "Eukaryota" in organism_type:
            return "Eukaryota"
        elif ("Bacteria" or "Archaea") in organism_type or ("Bacteria" or "Archaea") in scientific_name:
            return "Prokaryota"
        else:
            return "Unclassified"


def count_type(df):
    data = dict(zip(df['sequence_id'], df['Organism Type']))
    counts = {}
    for contig, org_type in data.items():
        contig_id, gene_id = contig.split("_")
        if contig_id in counts:
            if org_type in counts[contig_id]:
                counts[contig_id][org_type] += 1
            else:
                counts[contig_id][org_type] = 1
        else:
            counts[contig_id] = {org_type: 1}

    new_df = pd.DataFrame.from_dict(counts, orient='index').fillna(0).astype(int)
    new_df['Total'] = new_df.sum(axis=1)

    # Check if number of Viruses and Prokaryota are equal, then set Best_Hit to "Viruses"
    if 'Prokaryota'  in new_df.columns and 'Eukaryota'  in new_df.columns and 'Viruses' in new_df.columns :
        if new_df['Prokaryota'].equals(new_df['Eukaryota']) and new_df['Eukaryota'].equals(new_df['Viruses']):
            new_df['Best_Hit'] = "Viruses"
        elif new_df['Prokaryota'].equals(new_df['Viruses']) or new_df['Eukaryota'].equals(new_df['Viruses']):
            new_df['Best_Hit'] = "Viruses"
        else:
            new_df['Best_Hit'] = new_df.drop(['Total', 'Unclassified'], axis=1).idxmax(axis=1)

        virus_zero = (new_df[['Prokaryota', 'Viruses', 'Eukaryota']] == 0).all(axis=1)
        new_df.loc[virus_zero, 'Best_Hit'] = 'Unknown'

    elif 'Prokaryota' in new_df.columns and 'Viruses' in new_df.columns and 'Eukaryota' not in new_df.columns:
        if new_df['Prokaryota'].equals(new_df['Viruses']):
            new_df['Best_Hit'] = "Viruses"
        else:
            new_df['Best_Hit'] = new_df.drop(['Total', 'Unclassified'], axis=1).idxmax(axis=1)

        virus_zero = (new_df[['Prokaryota', 'Viruses']] == 0).all(axis=1)
        new_df.loc[virus_zero, 'Best_Hit'] = 'Unknown'

    elif 'Prokaryota' not in new_df.columns and  'Eukaryota' in new_df.columns and 'Viruses'  in new_df.columns :
        if new_df['Eukaryota'].equals(new_df['Viruses']):
            new_df['Best_Hit'] = "Viruses"
        else:
            new_df['Best_Hit'] = new_df.drop(['Total', 'Unclassified'], axis=1).idxmax(axis=1)
        virus_eukaryota_zero = (new_df[['Eukaryota', 'Viruses']] == 0).all(axis=1)
        new_df.loc[virus_eukaryota_zero, 'Best_Hit'] = 'Unknown'

    elif 'Prokaryota' in new_df.columns and 'Eukaryota' not in new_df.columns and 'Viruses' in new_df.columns:
        new_df['Best_Hit'] = new_df.drop(['Total', 'Unclassified'], axis=1).idxmax(axis=1)
        virus_zero = (new_df[['Viruses']] == 0).all(axis=1)
        new_df.loc[virus_zero, 'Best_Hit'] = 'Unknown'

    elif 'Viruses' not in new_df.columns and 'Eukaryota' not in new_df.columns and 'Prokaryota' in new_df.columns:
        new_df['Best_Hit'] = new_df.drop(['Total', 'Unclassified'], axis=1).idxmax(axis=1)
        proka_zero = (new_df[['Prokaryota']] == 0).all(axis=1)
        new_df.loc[proka_zero, 'Best_Hit'] = 'Unknown'


    else:
        new_df['Best_Hit'] = new_df.drop(['Total', 'Unclassified'], axis=1).idxmax(axis=1)
    new_df = new_df[['Best_Hit', 'Total'] + new_df.columns[:-2].tolist()]
    new_df = new_df.rename_axis('Contig_ID').reset_index()


    return new_df


def stat(df):
    """
    Count the number of organism type of contigs
    :param df: contigs dataframe
    :return: statistic table
    """
    data = dict(zip(df['Contig_ID'], df['Best_Hit']))
    counts = {}
    for contig, org_type in data.items():
        if org_type in counts:
            counts[org_type] += 1
        else:
            counts[org_type] = 1

    new_df = pd.DataFrame.from_dict(counts, orient='index', columns=["Counts"])
    new_df = new_df.rename_axis('Type').reset_index()

    return new_df


# Add a new column to the DataFrame indicating the organism type
df['Organism Type'] = df["taxonomy_id"].apply(get_organism_type)
new_df = pd.DataFrame({'sequence_id': df["sequence_id"]})
new_df["Organism Type"] = df["taxonomy_id"].apply(get_organism_type)

# Save the updated table to a new TSV file
df1 = count_type(new_df)
df2 = stat(df1)
output_filename = args.output if args.output else args.filename.replace('.tsv', '_updated.xlsx')
with pd.ExcelWriter(output_filename) as writer:
    df1.to_excel(writer, sheet_name='taxonomy')
    df2.to_excel(writer, sheet_name='global info')
    new_df.to_excel(writer, sheet_name='taxonomy detail')

