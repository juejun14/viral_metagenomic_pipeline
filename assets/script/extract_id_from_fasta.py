# -*- coding: utf-8 -*-
"""
@Time: 29/06/23 9:38 AM
@Author: Juejun CHEN
"""
from Bio import SeqIO,SwissProt


class extract_taxonID:
    """
    The class is used to generate a taxon id list
    in txt format from a fasta file, which will
    be used to filter a mmseqs2 taxonomy database.
    """
    def __init__(self):
        self.db = ''
        self.new_db = ''
        self.seq_dict = {}

    def import_file(self, db_file):
        """
        transfer fsa file to dictionary

        :param db_file: protein sequence file (.fsa or .faa)
        :return: dictionary
        """

        with open(db_file) as f:
            line = f.readline()
            while line:
                if ">" in line:
                    key = line
                    self.seq_dict[key] = []
                    # print(key)
                else:
                    self.seq_dict[key].append(line)
                    # print(line)
                line = f.readline()
        return self.seq_dict

    def check_keyword(self, word_list,  seq_dict, filter_name=False):
        """
        Generate the taxon id list of keyword in list format
        """
        new_list = []
        for k in seq_dict.keys():
            tax_name = k.split("Tax=")[1].split(" TaxID=")[0].split(" ")
            for i in tax_name:
                tax_name[tax_name.index(i)] = i.lower()
            for each in word_list:
                # if keyword is a number
                if each.isdigit() and each not in new_list:
                    new_list.append(each)
                # if keyword is a word
                elif not each.isdigit():
                    # if tax name presents a specie type
                    if len(tax_name) >= 2:
                        if each in tax_name:
                            # extract taxon id for sequence and add to new list
                            taxon_id = k.split("TaxID=")[1].split(" ")[0]
                            if taxon_id not in new_list:
                                new_list.append(taxon_id)
                        if filter_name:
                            if tax_name[-1]=="sp.":
                                taxon_id = k.split("TaxID=")[1].split(" ")[0]
                                if taxon_id not in new_list:
                                    new_list.append(taxon_id)

                    else:
                        if each in tax_name:
                            taxon_id = k.split("TaxID=")[1].split(" ")[0]
                           # print("no specie, ", tax_name, taxon_id, each, k)
                            if taxon_id not in new_list:
                                new_list.append(taxon_id)

        # print(new_list)
        # print("total taxon id:", len(new_list))
        return new_list

    def filter_only_list(self, list):
        """
        This function generates the string used to keep 
        only the sequences in the database that contain the id list.
        """
        merge_id = ''
        if len(list) > 1:
            merge_id = '||'.join(list)
        else:
            merge_id = ''.join(list)
        print(merge_id)
        return merge_id

    def split_list_by_n(self, list_collection, n):
        for i in range(0, len(list_collection), n):
            yield list_collection[i: i + n]

    def filter_not_list(self, list):
        """
        This function generates the string used to remove 
        the sequence containing the id list in the database. 
        """
        merge_id = ''
        merge_list=[]
        n=0
        if len(list) > 1000:
            for i in list:
                not_i = "!" + i
                list[list.index(i)] = not_i
            print(list)
            new_list = self.split_list_by_n(list, 1000)
            for sub_list in new_list:
                print(sub_list)
                merge_id = '&&'.join(sub_list)
                merge_list.append(merge_id)

            return merge_list
        elif 1 < len(list) <= 1000:
            for i in list:
                not_i = "!" + i
                list[list.index(i)] = not_i
            merge_id = '&&'.join(list)

            return merge_id
        else:
            merge_id = "!" + ''.join(list)

            print(merge_id)
            return merge_id


    def export_result(self,filename, result):
        """
        Save taxon id list into a txt file
        """
        with open(filename, "w") as f:
            if type(result)==str:
                f.write(result)
            elif type(result)==list:
                for line in result:
                    f.writelines(line)
                    f.write("\n")


if __name__ == '__main__':
    # Defined the output file name
    filename = "result/taxon_id_v5.txt"

    ## Defined the keyword list
    keyword_list = ["metagenomic", "metagenome", "uncultured", "bacterium", "candidate", "candidatus", "archaeon",  "35493", "33208"]

    # Defined the original fasta file to extract the taxon id list
    file='/shared/projects/viropip/test/uniref90.fasta'

    filter_DB = extract_taxonID()
    seq_dict = filter_DB.import_file(file)
    taxon_list = filter_DB.check_keyword(keyword_list, seq_dict,filter_name=True)
    result = filter_DB.filter_not_list(taxon_list)
    filter_DB.export_result(filename, result)

