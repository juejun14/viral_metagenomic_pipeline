# -*- coding: utf-8 -*-
"""
@Time: 4/18/23 3:48 PM
@Author: Juejun CHEN
"""
import argparse
import pandas as pd

# Create an argument parser to accept the filename as an argument
parser = argparse.ArgumentParser(description='Convert HMMER tblout to Pandas DataFrame')
parser.add_argument('filename', type=str, help='Path to HMMER tblout file')
parser.add_argument('-o', '--output', type=str, default='output.xlsx', help='Output filename')

# Parse the command-line arguments
args = parser.parse_args()

# Open the file specified by the user
with open(args.filename) as f:
    # Skip lines until we reach the start of the table
    for line in f:
        if line.startswith('#'):
            continue
        else:
            break

    # Read the table into a Pandas DataFrame
    data = []
    for line in f:
        # Split the line into fields
        fields = line.strip().split()[0:18]
        description = " ".join(line.strip().split()[18:-1])
        fields.append(description)

        # If the line doesn't have the expected number of fields, skip it
        if len(fields) != 19:
            continue

        # Otherwise, append the fields to the data list
        interest = fields[0:3]
        interest.append(fields[4])
        interest.append(fields[7])
        interest.append(fields[-1])

        data.append(interest)


    # Create the DataFrame from the data list
    df = pd.DataFrame(data)

# Rename the columns to match the format described in the HMMER documentation
df.columns = ['target_name', 'target_accession','query_name',
              'full_sequence_evalue', 'dominance_evalue',
              'description of target']

# Move the 'query_name' column to the first position
query_name_column = df.pop('query_name')
df.insert(0, 'query_name', query_name_column)

# Save the resulting DataFrame to a xlsx file
df.to_excel(args.output, index=False)

# Print a message to confirm that the file was saved
print(f'Saved DataFrame as {args.output}')
