#!/bin/bash
#SBATCH --partition=long
#SBATCH --ntasks=1
##SBATCH --nodes=1
#SBATCH --mem=200GB
##SBATCH --time=0-04:00
#SBATCH --cpus-per-task=32
#SBATCH -o result/pipeline_outputs/slurm.%N.%j.out
#SBATCH -e result/pipeline_outputs/slurm.%N.%j.err
#SBATCH --mail-type END
#SBATCH --job-name=mmseqsDB
#SBATCH -A viropip


cd /shared/projects/viropip/




module load python

## Create the taxon_id list in txt format
python3 script/extract_id_from_fasta.py


## MMseqs2 DB formatted
mmseqs2DB=$mmseqs2DB ## /shared/projects/phycovir/FORMATED_DB/UniRef90/UniRef90

## Filter the existant taxDB with taxon_id file
module load mmseqs2

taxon_id="result/taxon_id_v5.txt" ## the path should be the same as the output of extract_id_from_fasta.py
n=1
k=1
length=`grep "" $taxon_id |wc -l`

echo "cat file while read line"
cat $taxon_id | while read line
do
    if [ ${n} = 1 ];then
        keyword=${line}
        echo "command 1"
        mmseqs filtertaxseqdb ${mmseqs2DB} mmseq2DB/uniref90_cleaned_DB_part_${n} --taxon-list ${keyword}
	mkdir result/tmp_createdb_${n}
	mmseqs createtaxdb mmseq2DB/uniref90_cleaned_DB_part_${n} result/tmp_createdb_${n}
        n=`expr ${n} + 1`
    else
	if [ ${k} = 1 ];then
	    rm -r result/tmp_createdb_${k}
	fi

	keyword=${line}
        mmseqs filtertaxseqdb mmseq2DB/uniref90_cleaned_DB_part_${k} mmseq2DB/uniref90_cleaned_DB_part_`expr ${k} + 1` --taxon-list ${keyword}
        mkdir result/tmp_createdb_${k}
        mmseqs createtaxdb mmseq2DB/uniref90_cleaned_DB_part_`expr ${k} + 1` result/tmp_createdb_${k}
        k=`expr ${k} + 1`
	rm mmseq2DB/uniref90_cleaned_DB_part_`expr ${k} - 1`*
    fi
done



