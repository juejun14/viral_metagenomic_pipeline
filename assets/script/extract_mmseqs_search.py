# -*- coding: utf-8 -*-
"""
@Time: 5/30/23 11:54 AM
@Author: Juejun CHEN
"""

import argparse
import pandas as pd

# Create an argument parser to accept the filename as an argument
parser = argparse.ArgumentParser(description='Create summary table from specified column in a CSV file.')
parser.add_argument('input_file', type=str, help='path to the input CSV file')
parser.add_argument('output_file', type=str, help='path to the output summary CSV file')

# Parse the command-line arguments
args = parser.parse_args()


def create_summary_table(input_file, output_file):
    # read data
    data = pd.read_csv(input_file, delimiter='\t', header=None)

    # extract the index
    unique_values = data.iloc[:, 0].unique()
    print(unique_values)
    print(len(unique_values))
    # create an empty dataframe to store the results
    summary_data = pd.DataFrame()
    # print(summary_data)

    # extract the specify columns
    columns_to_extract = [0, 1, 17, 18]

    # print(data.iloc[:, columns_to_extract])

    for value in unique_values:
        row = data[data.iloc[:, 0] == value].iloc[0, columns_to_extract]  # extract the first line of each contig
        summary_data = pd.concat([summary_data, row.to_frame().T], ignore_index=True)

    print(summary_data)
    # export result
    summary_data.to_excel(output_file, index=False)
    print("Summary table created successfully.")


create_summary_table(args.input_file, args.output_file)
