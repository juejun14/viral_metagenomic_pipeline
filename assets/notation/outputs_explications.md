# Explications for output files

Written at 05 May 2023 by Juejun CHEN.

----------------------------------------------------------------------

This is a summary document that helps to understand the output results from this pipeline. The description of results is from the user guide of each tools.

## Quality control

- FastP
- FastQC
- Bowtie2

### FastP

This step will generate a report in format html and text.  
It will remove adapter by auto-detected the overlap zone or specify the sequence of adapter in options

### FastQC

This step will generate 3 kinds of results:

- FastQC report for R1,R2 before trimming and removal host genome 
- FastQC report for R1,R2 after trimming and removal host genome
- a log file when there are errors, if not it is empty

This part helps to take a global view of the quality of raw reads before and after the preprocessing.

### Bowtie2

This step will generate a log file to present the overall alignment rate.  
If the host genome is accessible, it will align firstly the host genome to raw reads.  
If not, it will align with the genome of Phix (used for Illumina sequencing) and remove it from raw reads.

----------------------------------------------------------------------

## Assembly

### SPAdes

In this part, you will obtain these files:

- contig.fastq.gz
- scaffold.fastq.gz 
- scaffold.rename.fastq.gz : the header of sequences are renamed and numerated
- depth.txt
- coverage.txt
- bowtie2 align.log
- SPAdes.log

The depth and coverage of contigs are calculted by samtools.

#### Depth

The output of depth has 3 columns, the first is the name of contig, the second is the position and the third is the number of reads aligned at that position.


#### Coverage

For the coverage output, here is the header for each column:

- **rname**	Reference name / chromosome
- **startpos**	Start position
- **endpos**	End position (or sequence length)
- **numreads**	Number reads aligned to the region (after filtering)
- **covbases**	Number of covered bases with depth >= 1 
- **coverage**	Percentage of covered bases [0..100]
- **meandepth**	Mean depth of coverage 
- **meanbaseq**	Mean baseQ in covered region 
- **meanmapq**	Mean mapQ of selected reads


The bowtie2 is used to compare the aligned contigs on raw reads and return a report as result.


#### Binning (by Metabat2)

This step uses to separate the contigs by genome.
You will obtain 3 files in general:
- depth.txt : the depth of sequencing
- binning_error.log: report the binning error.
- binning_report.log: report the binning result.


If the sequencing data does not have enough depth or coverage, it's possible you obtain 0 bin as output.

By default, the minimum length of contigs should be more than 1500. If the contigs are too short, it's also possible that you get 0 bin.

For more information, pleace check the **<a href=https://bitbucket.org/berkeleylab/metabat/src/master/>Metabat2 documentation </a>**. 


### QUAST

This part generate a report in pdf and html of assembly result.
You can find out the statistic information here, such as the total number of assembly contigs, the shortest and longest contigs, etc.


## Prokka

- **.gff** This is the master annotation in GFF3 format, containing both sequences and annotations. It can be viewed directly in Artemis or IGV.
- **.gbk** This is a standard Genbank file derived from the master .gff. If the input to prokka was a multi-FASTA, then this will be a multi-Genbank, with one record for each sequence.
- **.fna** Nucleotide FASTA file of the input contig sequences.
- **.faa** Protein FASTA file of the translated CDS sequences.
- **.ffn** Nucleotide FASTA file of all the prediction transcripts (CDS, rRNA, tRNA, tmRNA, misc_RNA)
- **.sqn** An ASN1 format "Sequin" file for submission to Genbank. It needs to be edited to set the correct taxonomy, authors, related publication etc.
- **.fsa** Nucleotide FASTA file of the input contig sequences, used by "tbl2asn" to create the .sqn file. It is mostly the same as the .fna file, but with extra Sequin tags in the sequence description lines.
- **.tbl** Feature Table file, used by "tbl2asn" to create the .sqn file.
- **.err** Unacceptable annotations - the NCBI discrepancy report.
- **.log** Contains all the output that Prokka produced during its run. This is a record of what settings you used, even if the --quiet option was enabled.
- **.txt** Statistics relating to the annotated features found.
- **.tsv** Tab-separated file of all features: locus_tag,ftype,len_bp,gene,EC_number,COG,product  

In general, the output of prokka is named according to the number of the total number of genes. 
It is not easy to classify genes by contig in this case.
In order to make it easier for users to find the gene sequence of interest, 
the faa file is associate with the gff file to regenerate a file with more 
detailed information in the header, called **rename.faa**.

Here is an example:
```
# Before
>LIEOHMCL_00001 hypothetical protein

# After
>contig1_00001 347-916_hypothetical_protein_gene-length570_cov-length9061
```

The header corresponds to the associated contig and numerate the gene on this contig.
"347-916" is the position of start and end of this gene on the contig.
You can find out the length of gene is 570 and the total length of contig is 9061.


For more information, pleace check the **<a href=https://github.com/tseemann/prokka>Prokka documentation </a>**. 

----------------------------------------------------------------------

## MMseqs2 Taxonomy Assignment

In this part you will obtain 5 files as outputs:
- taxonomy report in format .html (krona presentation)
- taxonomy report in format text
- taxonomy report in format .tsv
- contigs type report in format xlsx
- a log file when there are errors, if not it is empty.

### For the taxonomy report.tsv

Each line of the result file *.tsv will contain a tab separated list of 

1) query accession
2) LCA NCBI taxon ID
3) LCA rank name
4) LCA scientific name 


### For the taxonomy report

Here is an example of taxonomy report output:
```commandline
73.7778	664	664	no rank	0	unclassified
26.2222	236	50	no rank	1	root
13.6667	123	12	no rank	131567	  cellular organisms
12.1111	109	30	superkingdom	2	    Bacteria
4.8889	44	10	phylum	1224	      Proteobacteria
1.6667	15	0	class	1236	        Gammaproteobacteria
1.1111	10	0	order	135622	          Alteromonadales
1.1111	10	0	family	72275	            Alteromonadaceae
```
The column are (1) the percent of reads covered by the clade rooted at this taxon, (2) number of reads covered by the clade rooted at this taxon, (3) number of reads assigned directly to this taxon, (4) rank, (5) taxonomy identifier, and (6) scientific name. See <a href=https://ccb.jhu.edu/software/kraken/MANUAL.html#sample-reports>kraken documentation. </a>


### For the contigs type 

It includes 3 tables, the number of gene for each contigs will be counted. 
This table uses to count the type of each gene on each contig and try to predict the type of contigs. The taxonomic information of gene is from the NCBI database.
For each contig, the "BEST_HIT" column correspond to the max counted of phylum. 
Note, in this table, if the number of each domain is equal, the contigs is considered as viruses.

For more information, you can check directly the MMseqs **<a href="https://mmseqs.com/latest/userguide.pdf"> user guide </a>**

----------------------------------------------------------------------
## Functional annotations

This part use two different method to annotate the predicted gene:

- MMseqs2 (Swiss-prot)
- Hmmer (Pfam)

### MMseqs2 Search

The result of mmseqs search is convert into a tsv file called "*.convertalis.tsv", it includes multiples columns.

The header of each column refers: (in order)

- **query** Query sequence identifier  
- **target** Target sequence identifier  
- **theader** Header of Target sequence
- **pident**  Percentage of identical matches
- **alnlen** Alignment length (number of aligned columns)
- **mismatch** Number of mismatches
- **gapopen** Number of gap open events (note: this is NOT the number of gap characters)
- **qstart** 1-indexed alignment start position in query sequence
- **qend** 1-indexed alignment end position in query sequence
- **tstart** 1-indexed alignment start position in target sequence
- **tend** 1-indexed alignment end position in target sequence
- **evalue** E-value
- **bits** Bit score
- **empty** Dash column ‘-’
- **qlen** Query sequence length
- **tlen**  Target sequence length
- **taxid**  Taxonomical identifier (needs mmseqs tax db)
- **taxname** Taxon Name (needs mmseqs tax db)
- **taxlineage** Taxonomical lineage (needs mmseqs tax db)



### HMMER   

This part will generate 3 files:

- hmmscan_result.txt
- hmmscan_result.tbl
- a log file when there are errors, if not it is empty.

Here is the legend for each column in **hmmscan_result.tbl**:

* **target name** The name of the target sequence or profile.

* **accession** The accession of the target sequence or profile, or '-' if none.  
 
* **query name** The name of the query sequence or profile.  

* **accession** The accession of the query sequence or profile, or '-' if none.

* **E-value (full sequence)** The expectation value (statistical significance) of the target. This is a per query E-value; i.e. calculated as the expected number of false positives achieving this comparison’s score for a single query against the Z sequences in the target dataset. If you search with multiple queries and if you want to control the overall false positive rate of that search rather than the false positive rate per query, you will want to multiply this perquery E-value by how many queries you’re doing.

* **score (full sequence)** The score (in bits) for this target/query comparison. It includes the biased-composition correction (the “null2” model).

* **Bias (full sequence)** The biased-composition correction: the bit score difference contributed by the null2 model. High bias scores may be a red flag for a false positive, especially when the bias score is as large or larger than the overall bit score. It is difficult to correct for all possible ways in which a nonrandom but nonhomologous biological sequences can appear to be similar, such as short-period tandem repeats, so there are cases where the bias correction is not strong enough (creating false positives).

* **E-value (best 1 domain)** The E-value if only the single best-scoring domain envelope were found in the sequence, and none of the others. If this E-value isn’t good, but the full sequence E-value is good, this is a potential red flag. Weak hits, none of which are good enough on their own, are summing up to lift the sequence up to a high score. Whether this is Good or Bad is not clear; the sequence may contain several weak homologous domains, or it might contain a repetitive sequence that is hitting by chance (i.e. once one repeat hits, all the repeats hit).

* **score (best 1 domain)** The bit score if only the single best-scoring domain envelope were found in the sequence, and none of the others. (Inclusive of the null2 bias correction.]

* **bias (best 1 domain)** The null2 bias correction that was applied to the bit score of the single best-scoring domain.

* **exp** Expected number of domains, as calculated by posterior decoding on the mean number of begin states used in the alignment ensemble.

* **reg** Number of discrete regions defined, as calculated by heuristics applied to posterior decoding of begin/end state positions in the alignment ensemble. The number of regions will generally be close to the expected number of domains. The more different the two numbers are, the less discrete the regions appear to be, in terms of probability mass. This usually means one of two things. On the one hand, weak homologous domains may be difficult for the heuristics to identify clearly. On the other hand, repetitive sequence may appear to have a high expected domain number (from lots of crappy possible alignments in the ensemble, no one of which is very convincing on its own, so no one region is discretely well-defined).

* **clu** Number of regions that appeared to be multidomain, and therefore were passed to stochastic traceback clustering for further resolution down to one or more envelopes. This number is often zero.

* **ov** For envelopes that were defined by stochastic traceback clustering, how many of them overlap other envelopes.

* **env** The total number of envelopes defined, both by single envelope regions and by stochastic traceback clustering into one or more envelopes per region.

* **dom** Number of domains defined. In general, this is the same as the number of envelopes: for each envelope, we find an MEA(maximum expected accuracy) alignment, which defines the endpoints of the alignable domain.

* **rep** Number of domains satisfying reporting thresholds. If you’ve also saved a --domtblout file, there will be one line in it for each reported domain. 

* **inc** Number of domains satisfying inclusion thresholds.

* **description of target** The remainder of the line is the target’s description line, as free text.


In **hmmscan_result.txt**, you can find out the alignment detail for each domain.


For more information, you can check the  **<a href=http://eddylab.org/software/hmmer/Userguide.pdf>HMMER user guide </a>**

